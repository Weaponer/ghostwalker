﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class CompleteLevel : EventGame
{
    Stopwatch timer;

    bool startBlackout;
    public override void Update()
    {
        if (timer.ElapsedMilliseconds > 2000)
        {
            CallEndEvent();
        }
        else if (!startBlackout && timer.ElapsedMilliseconds > 1500)
        {
            CameraControll.Singleton.Blackout.Blackout();
            startBlackout = true;
        }
    }

    protected override void End()
    {
        LevelInfoUI.LevelInfo.HideFinalLevel();
        SceneLoad.LevelPassed();
    }

    public override void StartEvent()
    {
        LevelInfoUI.LevelInfo.FinalLevel();
        timer = new Stopwatch();
        timer.Start();

        bool[] param = new bool[GameHandler.Singleton.Level.GetNumerStage()];
        for (int i = 0; i < param.Length; i++)
            param[i] = true;
        LevelInfoUI.LevelInfo.SetParamsLevelIcon(param);
    }
}
