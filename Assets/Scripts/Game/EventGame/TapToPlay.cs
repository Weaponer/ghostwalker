﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapToPlay : EventGame
{


    public override void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CallEndEvent();
        }
    }

    protected override void End()
    {
        Pause.OffPause();
        TapToPlayTextUI.TapToPlayText.HideText();
    }

    public override void StartEvent()
    {
        Pause.SetPause();
        TapToPlayTextUI.TapToPlayText.ShowText();
    }
}
