﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class FaildLevel : EventGame
{
    Stopwatch timer;

    bool startBlackout;
    public override void Update()
    {
        if (timer.ElapsedMilliseconds > 4000)
        {
            CallEndEvent();
        } else if ( !startBlackout&&timer.ElapsedMilliseconds > 3500) {
            CameraControll.Singleton.Blackout.Blackout();
            startBlackout = true;
        }
    }

    protected override void End()
    {
        LevelInfoUI.LevelInfo.HideLevelFaild();
        GameHandler.Singleton.Level.Restart();
    }

    public override void StartEvent()
    {
        LevelInfoUI.LevelInfo.LevelFaild();
        TimeControll.Singleton.ChannelTimeTo(TimeControll.TimeNormal);
        timer = new Stopwatch();
        timer.Start();

        EventSendControll.LevelResultEvent(true, TimerStage.GetRecrodS(), GameHandler.Singleton.Level.GetNumLevel(), GameHandler.Singleton.Level.GetNumStage());
    }
}
