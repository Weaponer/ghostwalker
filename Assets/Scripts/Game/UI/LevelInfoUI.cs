﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelInfoUI : MonoBehaviour
{
    public static LevelInfoUI LevelInfo;

    [SerializeField] private Text levelInfo;

    [SerializeField] private Transform iconStage;
    [SerializeField] private StageIconUI prefabIcon;

    [SerializeField] private Text finalLevel;
    [SerializeField] private Text stageComplite;
    [SerializeField] private Text levelFaild;


    private List<StageIconUI> icons = new List<StageIconUI>();
    private void Awake()
    {
        if (LevelInfo)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            LevelInfo = this;
        }
    }

    public void SetNameLevel(int numer)
    {
        levelInfo.text = "LEVEL " + numer.ToString();
    }

    public void ResetIconTo(int count)
    {
        if (icons.Count > count)
        {
            int num = icons.Count - count;
            for (int i = 0; i < num; i++)
            {
                StageIconUI stage = icons[0];
                icons.Remove(stage);
                Destroy(stage.gameObject);
            }
        }
        else if (icons.Count < count)
        {
            int num = count - icons.Count;
            for (int i = 0; i < num; i++)
            {
                StageIconUI uI = Instantiate(prefabIcon, iconStage);
                uI.gameObject.SetActive(true);
                uI.Off();
                icons.Add(uI);
            }
        }
        for (int i = 0; i < icons.Count; i++) {
            icons[i].Off();
        }
    }

    public void SetParamsLevelIcon(bool[] param)
    {
        for (int i = 0; i < icons.Count; i++)
        {
            if (param.Length > i)
            {
                if (param[i])
                    icons[i].On();
                else
                    icons[i].Off();
            }
        }
    }

    public void HideFinalLevel()
    {
        finalLevel.gameObject.SetActive(false);
    }

    public void HideFinalStage()
    {
        stageComplite.gameObject.SetActive(false);
    }

    public void HideLevelFaild()
    {
        levelFaild.gameObject.SetActive(false);
    }

    public void FinalLevel()
    {
        finalLevel.gameObject.SetActive(true);
        stageComplite.gameObject.SetActive(false);
        levelFaild.gameObject.SetActive(false);
    }

    public void FinalStage()
    {
        stageComplite.gameObject.SetActive(true);
        finalLevel.gameObject.SetActive(false);
        levelFaild.gameObject.SetActive(false);
    }

    public void LevelFaild()
    {
        levelFaild.gameObject.SetActive(true);
        finalLevel.gameObject.SetActive(false);
        stageComplite.gameObject.SetActive(false);
    }
}
