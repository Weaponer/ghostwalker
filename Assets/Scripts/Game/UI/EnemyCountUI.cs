﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCountUI : MonoBehaviour
{
    public static EnemyCountUI EnemyCount;

    [SerializeField] private Text text;

    private void Awake()
    {
        if (EnemyCount)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            EnemyCount = this;
        }
    }

    public void SetCount(int count)
    {
        text.text = count.ToString();
    }
}
