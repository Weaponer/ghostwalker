﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageIconUI : MonoBehaviour
{
    [SerializeField] private GameObject trueIcon;
    public void Off()
    {
        trueIcon.SetActive(false);
    }

    public void On()
    {
        trueIcon.SetActive(true);
    }
}
