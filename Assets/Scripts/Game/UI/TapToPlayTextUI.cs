﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TapToPlayTextUI : MonoBehaviour
{
    public static TapToPlayTextUI TapToPlayText;

    [SerializeField] private Text text;

    private void Awake()
    {
        if (TapToPlayText)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            TapToPlayText = this;
        }
    }

    public void ShowText()
    {
        text.gameObject.SetActive(true);
    }

    public void HideText()
    {
        text.gameObject.SetActive(false);
    }
}
