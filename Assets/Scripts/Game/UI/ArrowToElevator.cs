using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowToElevator : MonoBehaviour
{
    public static ArrowToElevator Singleton { get; private set; }


    [SerializeField] private Transform arrow;

    [SerializeField] private GameObject arrowSprite;
    private void Awake()
    {
        if (Singleton)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            Singleton = this;
        }
    }

    public void Show()
    {
        arrowSprite.SetActive(true);
    }

    public void SetPositionAndDirect(Vector3 direct)
    {
        float zRotate = -Quaternion.LookRotation(direct, Vector3.up).eulerAngles.x;
        transform.eulerAngles = new Vector3(0, 0, zRotate);
    }



    public void Hide()
    {
        arrowSprite.SetActive(false);
    }

    private void OnDestroy()
    {
        if (Singleton == this)
        {
            Singleton = null;
        }
    }
}
