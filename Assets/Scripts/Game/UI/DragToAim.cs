using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragToAim : MonoBehaviour
{
    public static DragToAim Singleton { get; private set; }

    [SerializeField] private GameObject uiObject;

    private void Awake()
    {
        if (Singleton)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            Singleton = this;
        }
    }

    public void Show()
    {
        uiObject.SetActive(true);
    }


    public void Hide()
    {
        uiObject.SetActive(false);
    }

    private void OnDestroy()
    {
        if (Singleton == this)
        {
            Singleton = null;
        }
    }
}
