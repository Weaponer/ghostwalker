﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public PlayerStateController StateController { get; private set; }

    [Range(0f, 1000f)]
    [SerializeField] private float forceJumpMax;

    [Range(0f, 1000f)]
    [SerializeField] private float forceJumpMin;

    private HoverJump hoverJump;


    private PlayerPhysics playerPhysics;

    [SerializeField] private Rigidbody rigidbody;

    [SerializeField] private EntityBlood bloodEffect;
    [SerializeField] private JumpExplosion jumpExplosion;
    [SerializeField] private ContactPhysics contactPhysics;
    [SerializeField] private GroundDetect groundDetect;


    [SerializeField] private AnimationMaterial animationMaterial;
    private AnimationControll animationControll;

    [SerializeField] private TakeDamag takeDamag;
    [SerializeField] private CapsuleCollider EnemyAttackCollider;
    public AttackEnemy AttackEnemys { get; private set; }

    [SerializeField] private SyncTransforms prefabPlayerRagdoll;
    [SerializeField] private Transform root;

    private Ragdoll ragdoll;

    bool dead;

    private void Awake()
    {
        StateController = new PlayerStateController();

        hoverJump = new HoverJump();

        hoverJump.SetForce(forceJumpMin, forceJumpMax);

        playerPhysics = new PlayerPhysics();
        playerPhysics.SetRigidbody(rigidbody, animationMaterial.PlayerCollider, contactPhysics, groundDetect);

        playerPhysics.PlayerConnect += PlayerContact;
        playerPhysics.PlayerDisconnect += PlayerDisconnect;

        animationControll = new AnimationControll();


        AttackEnemys = new AttackEnemy();
        AttackEnemys.Init(EnemyAttackCollider, this);

        animationControll.SetParams(this, playerPhysics, AttackEnemys, animationMaterial);

        takeDamag.Take += Dead;

        StateController.InitElements(this, playerPhysics);
    }

    private void Update()
    {
        if (!Pause.IsPause && !dead && !StateController.PlayerControll)
        {
            UpdateHoverJump();
            AttackEnemys.UpdateAttack();
        }
        else {
            if (hoverJump.Active)
            {
                hoverJump.Disable();
            }
        }
    }

    private void LateUpdate()
    {
        if (!dead)
            CameraControll.Singleton.SetPosition(transform.position);
        if (!dead && !StateController.PlayerControll)
        {
            if (!Pause.IsPause)
            {
                playerPhysics.MoveRunUpdate();
            }
        }
    }

    public void PlayerTeleport(Vector3 vector)
    {
        transform.position = vector;
        playerPhysics.ResetParams();
        animationControll.ResetAnimator();

        if (hoverJump.Active)
        {
            hoverJump.Disable();
        }
    }

    private void UpdateHoverJump()
    {
        if (hoverJump.Active)
        {
            if (hoverJump.CheckDisable())
            {
                TimeControll.Singleton.ChannelTimeTo(TimeControll.TimeNormal);
                hoverJump.Disable();

                if (playerPhysics.GetMove())
                {
                    playerPhysics.StopMove();
                }

                if (playerPhysics.CheckJumpInDirection(hoverJump.GetTrajectory().normalized) || !playerPhysics.CheckGroundContact())
                {
                    UpdateVelocity(hoverJump.GetTrajectory());
                    jumpExplosion.Play();
                }

                return;
            }
            hoverJump.Update(this);
        }
        else
        {
            if (hoverJump.CheckMouseClickHover(this))
            {
                hoverJump.StartDetectTrajectory();
                TimeControll.Singleton.ChannelTimeTo(TimeControll.SlowTime);
            }
        }
    }

    private void PlayerContact()
    {
        if (!playerPhysics.GetMove())
        {
            TimeControll.Singleton.ChannelTimeTo(TimeControll.SlowTime);
            Vector3 normal = playerPhysics.GetNormalContact();
            float dot = Vector3.Dot(Vector3.up, normal);
            if (dot <= 0.1f)
            {
                playerPhysics.PlayerStopFly();
            }
            else if (!playerPhysics.GetMove())
            {
                if (playerPhysics.CheckMoveOnNormal(normal))
                {
                    playerPhysics.CorrectVelocityOnNormal(normal);
                    playerPhysics.StartMove();
                }
                else
                {
                    playerPhysics.PlayerStopFly();
                }
            }
        }
    }

    private void PlayerDisconnect()
    {
        if (!playerPhysics.CheckContact())
        {
            TimeControll.Singleton.ChannelTimeTo(TimeControll.TimeNormal);
        }
    }

    private void UpdateVelocity(Vector3 velocity)
    {
        playerPhysics.SetVelocityFly(velocity);
    }

    private void Dead(float s)
    {
        GameHandler.Singleton.PlayerDead();

        root.gameObject.SetActive(false);
        animationMaterial.PlayerSkin.gameObject.SetActive(false);
        SyncTransforms up = Instantiate(prefabPlayerRagdoll, transform);

        up.SyncFrom(root);

        ragdoll = up.GetComponent<Ragdoll>();
        gameObject.layer = LayerMask.NameToLayer("EnemyDeadParts");
        Invoke("CorrectRagdoll", 0.1f);
        ragdoll.SetForceRigidbody(rigidbody);

        bloodEffect.ShowBlood();
        dead = true;
    }

    private void CorrectRagdoll()
    {
        ragdoll.SetMask(LayerMask.NameToLayer("EnemyDeadParts"));
    }

    public void Respawn() {
        Destroy(ragdoll.gameObject);
        gameObject.layer = LayerMask.NameToLayer("Player");
        root.gameObject.SetActive(true);
        animationMaterial.PlayerSkin.gameObject.SetActive(true);
        dead = false;
    }
}
