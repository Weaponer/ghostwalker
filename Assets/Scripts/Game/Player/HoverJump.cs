﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverJump
{
    public bool Active { get; private set; }



    private Vector3 force;

    private Vector2 posMouse;

    private const float timeDist = 0.2f;

    private float maxForce = 500f;
    private float minForce = 300f;

    public HoverJump() {
        Input.multiTouchEnabled = false;
    }

    public void SetForce(float minForce, float maxForce)
    {
        this.maxForce = maxForce;
        this.minForce = minForce;
    }

    public bool CheckMouseClickHover(Player player)
    {
        if (Input.GetMouseButtonDown(0))
        {
            return true;
        }
        return false;
    }

    public void StartDetectTrajectory()
    {
        Active = true;
        posMouse = Input.mousePosition;
        LineTrajectory.Singleton.ShowTrajectory();
    }

    public void Update(Player player)
    {
        Vector2 direct = posMouse - new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        direct += direct.normalized * minForce;

        Vector3 directFly = CameraControll.Singleton.TransformFromCameraToWorld(direct);
        if (directFly.magnitude < 4) {
            directFly = Vector3.up * minForce;
        }

        force = directFly.normalized;
        force *= Mathf.Clamp(directFly.magnitude, minForce, maxForce);
        force *= 1f / 20f;
        UpdatePoints(player);
    }

    public bool CheckDisable()
    {
        if (!Input.GetMouseButton(0) && Active)
        {
            return true;
        }
        return false;
    }

    public void Disable()
    {
        Active = false;
        LineTrajectory.Singleton.HideTrajectory();
    }

    public Vector3 GetTrajectory()
    {
        return force;
    }

    private void UpdatePoints(Player player)
    {
        Vector3 offestStart = new Vector3(0, -1f, 0);
        Vector3[] pointsP = Trajectory.CreateTrajectory(10, 0.05f, force.magnitude, force.normalized, 0.005f);
        for (int i = 0; i < pointsP.Length; i++)
        {
            pointsP[i] += player.transform.position + offestStart;
        }
        LineTrajectory.Singleton.UpdateTrajectory(pointsP);
    }
}
