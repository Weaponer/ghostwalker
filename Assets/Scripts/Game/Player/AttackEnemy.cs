﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackEnemy
{
    public event System.Action PlayerAttack;

    Player player;
    CapsuleCollider capsule;

    public void Init(CapsuleCollider capsule, Player player)
    {
        this.player = player;
        this.capsule = capsule;
    }

    public void UpdateAttack()
    {
        Vector3 start = player.transform.position + new Vector3(0, ((capsule.height - 1) / 2f), 0);
        Vector3 end = player.transform.position - new Vector3(0, ((capsule.height - 1) / 2f), 0);

        if (Physics.CheckCapsule(start, end, capsule.radius, LayerMask.GetMask("Enemy")))
        {
            Enemy[] enemies = EnemyControll.Singleton.GetArray();
            float minDist = float.MaxValue;
            int index = 0;
            for (int i = 0; i < enemies.Length; i++)
            {
                float dist = Vector3.Distance(enemies[i].transform.position, player.transform.position);
                if (dist < minDist)
                {
                    minDist = dist;
                    index = i;
                }
            }
            if (minDist != float.MaxValue)
            {
                if (PlayerAttack != null)
                    PlayerAttack();
                Enemy enemy = enemies[index];
                enemy.Kill((enemy.transform.position - player.transform.position).normalized);
            }

        }

    }


}
