﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [SerializeField] protected Transform firePoint;

    [SerializeField] protected Transform baseWeapon;

    [SerializeField] protected Shell prefabShell;

    public void Fire()
    {
        ShellGenerator.CreateShell(prefabShell, this);
        CallFire();
    }

    protected virtual void CallFire()
    {

    }

    public Shell GetShell()
    {
        return prefabShell;
    }

    public Vector3 GetFireNormalize()
    {
        return firePoint.forward;
    }

    public Vector3 GetPosFire()
    {
        return firePoint.transform.position;
    }

    public void SetDirection(Vector3 normalize)
    {
        baseWeapon.rotation = Quaternion.LookRotation(normalize, Vector3.up);
    }
}
