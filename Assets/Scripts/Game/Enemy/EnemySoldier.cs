﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class EnemySoldier : Enemy
{
    [SerializeField] private Weapon weapon;

    [SerializeField] private Transform baseRoot;

    [SerializeField] private Transform syncRoot;

    [SerializeField] private SyncTransforms PrefabSoldier;

    [SerializeField] private EntityBlood blood;

    private Orientation orientation;
   [SerializeField] private AttackTarget attackTarget;

    Ragdoll ragdoll;

    protected override void Load()
    {
        orientation = new Orientation();
        orientation.Init(this);
        attackTarget.Init(weapon, this);
    }

    protected override void Tick(Player target)
    {
        orientation.UpdateTarget(target);
        attackTarget.UpdateTarget(target, Time.deltaTime);
    }

    protected override void CallKill(Vector3 direct)
    {
        blood.ShowBlood();

        baseRoot.gameObject.SetActive(false);
        SyncTransforms up = Instantiate(PrefabSoldier, transform);

        up.SyncFrom(syncRoot);


        Invoke("DestroyAll", 3f);

        ragdoll = up.GetComponent<Ragdoll>();
        ragdoll.AddForce(direct * 2000f);
        gameObject.layer = LayerMask.NameToLayer("EnemyDeadParts");
        Invoke("CorrectRagdoll", 0.1f);
    }

    private void CorrectRagdoll()
    {        
        ragdoll.SetMask(LayerMask.NameToLayer("EnemyDeadParts"));
    }

    private void DestroyAll()
    {
        Destroy(gameObject);
    }

    public override bool IsAttack()
    {
        return attackTarget.IsAttack;
    }
}
