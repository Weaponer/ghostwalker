﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemySoldierFly : EnemySoldier
{
    [SerializeField] private List<Transform> points = new List<Transform>();

   [SerializeField] private MoveTrajectory moveTrajectory;

    protected override void Load()
    {
        base.Load();
        moveTrajectory.Init(points.ToArray(), this);
    }

    protected override void Tick(Player target)
    {
        base.Tick(target);
        moveTrajectory.Update();
    }

    protected override void CallKill(Vector3 direct)
    {
        base.CallKill(direct);
    }

    [System.Serializable]
    private class MoveTrajectory
    {
        [SerializeField]
        private float speedMove;

        Transform[] points;

        Enemy enemy;

        int index;

        bool ret;

        public void Init(Transform[] points, Enemy enemy)
        {
            this.points = points;
            this.enemy = enemy;

            index = 0;
            ret = false;
            enemy.transform.position = points[0].position;
        }

        public void Update()
        {
            if (index == points.Length - 1)
            {
                ret = true;
            }
            else if (index == 0)
            {
                ret = false;
            }

            if (Vector3.Distance(points[index].position, enemy.transform.position) < 0.7f)
            {
                if (ret)
                {
                    index--;
                }
                else
                {
                    index++;
                }
            }

            enemy.transform.position += (points[index].position - enemy.transform.position).normalized * speedMove * Time.deltaTime;
        }
    }
}