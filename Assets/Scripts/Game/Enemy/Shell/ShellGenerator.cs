﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ShellGenerator
{
    public static void CreateShell(Shell prefab, Weapon weapon)
    {
        Shell shell = Object.Instantiate(prefab, weapon.GetPosFire(), Quaternion.LookRotation(weapon.GetFireNormalize(), Vector3.up));
        shell.SetParamsRigidbody();
        shell.StartFly(weapon.GetFireNormalize());
    }
}
