﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ShotResolver
{
    public static void Resolver(RaycastHit hit)
    {
        TakeDamag take = hit.collider.gameObject.GetComponent<TakeDamag>();
        if (take)
        {
            take.AddDamag();
        }
    }
}
