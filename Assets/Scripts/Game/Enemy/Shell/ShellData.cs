﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShellData
{
    public Rigidbody Phys;
    public float Velocity;
    public LayerMask Mask;
}
