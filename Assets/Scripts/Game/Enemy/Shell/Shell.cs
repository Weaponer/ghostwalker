﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Shell : MonoBehaviour
{
    [SerializeField] private ShellData shellData = new ShellData();

    public ShellData Data
    {
        get
        {
            return shellData;
        }
    }

    private bool isChecked;

    public void SetParamsRigidbody()
    {
        shellData.Phys.isKinematic = false;
        shellData.Phys.useGravity = false;
        shellData.Phys.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;
    }

    public void StartFly(Vector3 normalize)
    {
        isChecked = true;
        shellData.Phys.velocity = shellData.Velocity * normalize;
    }

    private void FixedUpdate()
    {
        if (isChecked)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, shellData.Phys.velocity.magnitude * Time.fixedDeltaTime * 2f, shellData.Mask))
            {
                ShotResolver.Resolver(hit);
                DestroyShell();
            }
        }
    }

    private void DestroyShell()
    {
        Destroy(gameObject);
    }

}
