﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackIcon : MonoBehaviour
{
    [SerializeField] private Image icon;

    [SerializeField] private Image circleRed;

    [SerializeField] private GameObject baseIcon;

    [SerializeField] private float timeLoad;

    private bool visible;

    float loadTime;

    Coroutine load;

    private void Awake()
    {
        visible = false;
        baseIcon.SetActive(false);
    }

    private void LateUpdate()
    {
        if (visible)
        {
            Transform camera = CameraControll.Singleton.transform;
            Vector3 pos = (transform.position - camera.position).normalized * 13f + camera.position;
            baseIcon.transform.position = pos;
            baseIcon.transform.rotation = Quaternion.LookRotation((transform.position - camera.position).normalized, Vector3.up);
        }
    }

    IEnumerator Load() {
        while (loadTime / timeLoad <= 1) {
            circleRed.fillAmount = loadTime / timeLoad;
            loadTime += Time.deltaTime;
            yield return null;
        }
        load = null;
    }

    public void HideIcon()
    {
        if (!visible)
            return;
        if (load != null)
        {
            StopCoroutine(load);
        }
        visible = false;
        baseIcon.SetActive(false);
        
    }

    public void ShowIcon()
    {
        if (visible)
            return;
        loadTime = 0;
        if (load != null) {
            StopCoroutine(load);
        }
        load = StartCoroutine(Load());

        visible = true;
        baseIcon.SetActive(true);
    }
}
