﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    [SerializeField] protected AttackIcon attackIconPrefab;
    [SerializeField] protected Vector3 posAttackIcon;

    public AttackIcon IconAttack { get; set; }

    private void Start()
    {
        IconAttack = Instantiate(attackIconPrefab, transform);
        IconAttack.transform.localPosition = posAttackIcon;
        EnemyControll.Singleton.AddEnemy(this);
        Load();
    }

    protected virtual void Load()
    {

    }

    public virtual bool IsAttack() {
        return false;
    }

    public void UpdateTick(Player target)
    {
        if (target != null)
            Tick(target);
    }

    protected virtual void Tick(Player target)
    {

    }

    public void Kill(Vector3 direct)
    {
        IconAttack.HideIcon();
        EnemyControll.Singleton?.RemoveEnemy(this);
        CallKill(direct);
    }

    protected virtual void CallKill(Vector3 direct)
    {

    }

    private void OnDestroy()
    {
        EnemyControll.Singleton?.RemoveEnemy(this);
    }
}

[System.Serializable]
public class AttackTarget
{
    public bool IsAttack { get; private set; }

    [SerializeField] private float speedAttack;
    Enemy enemy;
    Weapon weapon;

    float timer;

    float startTimer = 0;

    public void Init(Weapon weapon, Enemy enemy)
    {
        this.enemy = enemy;
        this.weapon = weapon;

        startTimer = GameHandler.Singleton.Level.Diffcultry.GetRandomTimeToStart();
    }

    public void UpdateTarget(Player target, float duration)
    {
        Vector3 dir = target.transform.position - weapon.transform.position;
        weapon.SetDirection(dir.normalized);

        if (CheckPlayerShot(target))
        {
            if (timer > speedAttack + startTimer)
            {
                weapon.Fire();
                timer = 0f;
                startTimer = 0;
            }
        }

        if (timer > speedAttack + startTimer - 1)
        {
            enemy.IconAttack.ShowIcon();
            IsAttack = true;
        }
        else {
            enemy.IconAttack.HideIcon();
            IsAttack = false;
        }
        timer += duration;
    }

    public bool CheckPlayerShot(Player target)
    {
        RaycastHit hit;
        if (Physics.Raycast(weapon.GetPosFire(), (target.transform.position - weapon.GetPosFire()).normalized, out hit, float.MaxValue, weapon.GetShell().Data.Mask))
        {
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                return true;
            }
        }
        return false;
    }
}

public class Orientation
{
    Enemy enemy;

    public void Init(Enemy enemy)
    {
        this.enemy = enemy;
    }

    public void UpdateTarget(Player target)
    {
        float x = Mathf.Sign((target.transform.position - enemy.transform.position).x);
        if (x > 0)
        {
            enemy.transform.eulerAngles = new Vector3(0, 90, 0);
        }
        else
        {
            enemy.transform.eulerAngles = new Vector3(0, -90, 0);
        }
    }
}