﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour
{
    [SerializeField] private Rigidbody[] rigidbodies;

    [SerializeField] private GameObject[] objects;

    private void FixedUpdate()
    {
        for (int i = 0; i < rigidbodies.Length; i++)
        {
            Vector3 vector = rigidbodies[i].transform.position;
            Vector2 border = GameHandler.Singleton.Level.GetCurrentStage().GetWidth();

            vector.z = Mathf.Clamp(vector.z, border.x, border.y);
            rigidbodies[i].position = vector;
        }
    }

    public void SetForceRigidbody(Rigidbody rb) {
        for (int i = 0; i < rigidbodies.Length; i++)
        {
            rigidbodies[i].velocity = rb.velocity;
            rigidbodies[i].angularVelocity = rb.angularVelocity;
        }
    }

    public void AddForce(Vector3 force)
    {
        for (int i = 0; i < rigidbodies.Length; i++)
        {
            rigidbodies[i].AddForce(force);
        }
    }
    public void SetMask(LayerMask mask)
    {
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].layer = mask;
        }
    }
}
