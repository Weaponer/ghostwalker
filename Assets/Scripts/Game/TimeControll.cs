﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeControll : MonoBehaviour
{

    public const float TimeNormal = 1f;

    public const float SlowTime = 0.105f;

    public static TimeControll Singleton { get; private set; }

    private Coroutine move;

    private float time;

    private void Awake()
    {
        if (Singleton)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            Singleton = this;
        }
        time = Time.timeScale;
    }

    public void ChannelTimeTo(float time)
    {
        if (move != null)
        {
            StopCoroutine(move);
        }
        move = StartCoroutine(MoveTime(time));
    }

    private IEnumerator MoveTime(float time)
    {
        float start = this.time;
        for (int i = 0; i < 15; i++)
        {
            if (!Pause.IsPause)
            {
                this.time = Mathf.Lerp(start, time, (float)i / 15f);
                Time.timeScale = this.time;
                Time.fixedDeltaTime = 0.02f * Time.timeScale;
            }
            else
            {
                i--;
            }
            yield return new WaitForSeconds(0.003f);
        }
        move = null;
    }

    private void OnDestroy()
    {
        if (Singleton == this)
        {
            Singleton = null;
        }
    }
}
