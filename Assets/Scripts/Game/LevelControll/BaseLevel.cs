using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseLevel : MonoBehaviour
{
    public event System.Action StageComplete;

    public event System.Action LevelComplete;

    public event System.Action Load;

    public event System.Action UnLoad;

    public LevelDiffcultry Diffcultry { get { return levelDiffcultry; } }

    [SerializeField] protected LevelDiffcultry levelDiffcultry = new LevelDiffcultry();

    [SerializeField] protected Player player;

    [SerializeField] protected Stage[] stages;

    protected int numStage;

    protected Stage currentStage;

    private int indexStage;

    public Stage GetCurrentStage()
    {
        return currentStage;
    }

    public Stage[] GetStages()
    {
        return stages;
    }

    public int GetNumerStage()
    {
        return numStage;
    }

    public void Restart()
    {
        UnLoadStage(currentStage);
        player.Respawn();
        LoadStage(indexStage);
    }

    public virtual void LoadNextStage()
    {
    }

    public virtual void StartGame()
    {
    }

    public virtual int GetNumLevel()
    {
        return 0;
    }

    public virtual int GetNumStage()
    {
        return 0;
    }

    protected void LoadStage(int num)
    {
        if (Load != null)
        {
            Load();
        }
        currentStage = Instantiate(stages[num], Vector3.zero, Quaternion.identity);
        currentStage.InitStage(player);
        CallLoadStage();
        TimerStage.StartRecord();

        indexStage = num;

    }

    protected virtual void CallLoadStage()
    {
    }

    protected void UnLoadStage(Stage stage)
    {
        stage.DisableStage();
        Destroy(stage.gameObject);
        CallUnLoadStage();
    }

    protected virtual void CallUnLoadStage()
    {

    }

    protected void LevelCompleteEvent()
    {
        if (LevelComplete != null)
            LevelComplete();
    }

    protected void StageCompleteEvent()
    {
        if (StageComplete != null)
            StageComplete();
    }
}
