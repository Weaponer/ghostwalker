﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelControll : BaseLevel
{

    public override void StartGame()
    {
        int l = 0;
        int s = 0;
        StaticGameObserver.LoadStartProgress(out l, out s);
        Level level = SceneLoad.GetLevelCurrent();
        int countLevel = SceneLoad.GetListLevels().Length;

        if (l > countLevel)
        {
            SceneLoad.LoadRepeatLevel();
        }
        else if (l != level.NumerLevel)
        {
            Level[] levels = SceneLoad.GetListLevels();
            for (int i = 0; i < levels.Length; i++)
            {
                if (levels[i].NumerLevel == l)
                {
                    SceneLoad.LoadLevel(levels[i]);
                    return;
                }
            }
        }

        LevelInfoUI.LevelInfo.SetNameLevel(SceneLoad.GetLevelCurrent().NumerLevel);
        LevelInfoUI.LevelInfo.ResetIconTo(stages.Length);

        bool[] param = new bool[s];
        for (int i = 0; i < param.Length; i++)
            param[i] = true;
        LevelInfoUI.LevelInfo.SetParamsLevelIcon(param);

        LoadStage(s);
        numStage = s;
    }

    private void StageFinish()
    {
        if (numStage < stages.Length - 1)
        {
            Level level = SceneLoad.GetLevelCurrent();
            numStage++;
            StaticGameObserver.SaveProgress(level.NumerLevel, numStage);
            StageCompleteEvent();
        }
        else
        {
            numStage++;
            
            Level currentLev = SceneLoad.GetLevelCurrent();
            StaticGameObserver.SaveProgress(currentLev.NumerLevel + 1, 0);

            LevelCompleteEvent();
        }
    }

    public override void LoadNextStage()
    {
        if (currentStage)
        {
            UnLoadStage(currentStage);
        }
        LoadStage(numStage);
    }

    protected override void CallLoadStage()
    {
        currentStage.StageComplit += StageFinish;

        EventSendControll.LevelStartEvent(SceneLoad.GetLevelCurrent().NumerLevel, numStage);
    }

    public override int GetNumLevel()
    {
        return SceneLoad.GetLevelCurrent().NumerLevel;
    }

    public override int GetNumStage()
    {
        return numStage;
    }
}
