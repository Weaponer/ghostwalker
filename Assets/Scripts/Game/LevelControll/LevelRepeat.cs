using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelRepeat : BaseLevel
{
    private int level;

    private int countStage;



    public override void StartGame()
    {
        int l = 0;
        int s = 0;
        StaticGameObserver.LoadStartProgress(out l, out s);

        level = l;
        CreateLevel(level);
    }

    private void StageFinish()
    {
        if (numStage < countStage - 1)
        {
            numStage++;
            StaticGameObserver.SaveProgress(level, numStage);
            StageCompleteEvent();
        }
        else
        {
            numStage++;
            LevelCompleteEvent();

            StaticGameObserver.SaveProgress(level + 1, 0);
        }
    }

    public void GenerateNextLevel() {
        
        level++;
        CreateLevel(level);
    }

    private void CreateLevel(int num) {
        countStage = Mathf.Min((int)(num / 10) + 2, 5);
        numStage = 0;
        if (currentStage)
            UnLoadStage(currentStage);
        LoadStage(Random.Range(0, stages.Length));

        LevelInfoUI.LevelInfo.SetNameLevel(num);
        LevelInfoUI.LevelInfo.ResetIconTo(countStage);

        bool[] param = new bool[0];
        LevelInfoUI.LevelInfo.SetParamsLevelIcon(param);

    }

    protected override void CallLoadStage()
    {
        currentStage.StageComplit += StageFinish;

        EventSendControll.LevelStartEvent(level, numStage);
    }

    public override int GetNumLevel()
    {
        return level;
    }

    public override int GetNumStage()
    {
       return numStage;
    }

    protected override void CallUnLoadStage()
    {

    }

    public override void LoadNextStage()
    {
        if (currentStage)
            UnLoadStage(currentStage);
        LoadStage(numStage);
    }
}
