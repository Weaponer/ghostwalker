using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailCatana : MonoBehaviour
{


    [SerializeField] private TrailRenderer trail;

    [SerializeField] private Player player;

    private void Start()
    {
        player.AttackEnemys.PlayerAttack += StartAttack;
        HideAnim();
    }

    private void StartAttack() {
        ShowAnim();
        Invoke("HideAnim", 0.4f);
    }

    private void ShowAnim() {
        trail.enabled = true;
    }

    private void HideAnim() {
        trail.enabled = false;
    }
}
