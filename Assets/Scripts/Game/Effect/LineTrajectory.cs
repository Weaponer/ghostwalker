﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineTrajectory : MonoBehaviour
{
    public static LineTrajectory Singleton { get; private set; }

    [SerializeField] private LineRenderer baseLine;

    [SerializeField] private Transform point;

    private void Awake()
    {
        if (Singleton)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            Singleton = this;
        }
    }

    public void UpdateTrajectory(Vector3[] worldPoint)
    {
        LayerMask layerMap = LayerMask.GetMask("Map");

        int maxCount = worldPoint.Length;
        RaycastHit hit;

        for (int i = 1; i < worldPoint.Length; i++)
        {
            if (Physics.Raycast(worldPoint[i - 1], worldPoint[i] - worldPoint[i - 1], out hit, Vector3.Distance(worldPoint[i], worldPoint[i - 1]), layerMap, QueryTriggerInteraction.Collide))
            {
                if (i > 1)
                {
                    maxCount = i;
                    worldPoint[i - 1] = hit.point;
                }
                else
                {
                    maxCount = 2;
                    worldPoint[1] = hit.point;
                }
                break;
            }
        }
        baseLine.positionCount = maxCount;
        baseLine.SetPositions(worldPoint);
        for (int i = 0; i < maxCount; i++)
        {
            Vector3 newPos = baseLine.GetPosition(i);
            Transform camera = CameraControll.Singleton.GameCamera.transform;
            Vector3 direct = newPos - camera.position;
            direct = direct.normalized;

            newPos = direct * 14f + camera.position;
            baseLine.SetPosition(i, newPos);
        }
        point.transform.position = baseLine.GetPosition(maxCount - 1);
    }

    public void HideTrajectory()
    {
        baseLine.gameObject.SetActive(false);
        point.gameObject.SetActive(false);
    }

    public void ShowTrajectory()
    {
        baseLine.gameObject.SetActive(true);
        point.gameObject.SetActive(true);
    }

    private void OnDestroy()
    {
        if (Singleton == this)
        {
            Singleton = null;
        }
    }
}
