using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpExplosion : MonoBehaviour
{

    [SerializeField] private ParticleSystem jumpEffect;
    public void Play()
    {
        jumpEffect.Play();
    }
}
