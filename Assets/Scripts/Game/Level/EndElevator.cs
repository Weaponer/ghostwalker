using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndElevator : StageEvent
{

    public event System.Action PlayerExitFromStage;

    [SerializeField] private Vector2 zoneZ;
    [SerializeField] private float posZoneY;
    [SerializeField] private float maxRadiusToZone;

    [SerializeField] private Transform elevatorEnd;

    [Space(15)]
    [SerializeField] private float delayToStartElevator;

    [SerializeField] private Transform elevator;

    [SerializeField] private Vector3 startLocalPos;

    [SerializeField] private Vector3 endLocalPos;

    [SerializeField] private Vector3 playerPosOnElevator;

    [SerializeField] private float timeUp;

    bool start;

    Coroutine anim;

    protected override void CallLoadStage()
    {

    }

    protected override void CallUpdateStage()
    {
        if (stage.PlayerStage != null && stage.GetEnemyOver() && !start)
        {
            Transform transformP = stage.PlayerStage.transform;
            if (stage.PlayerStage != null)
            {
                if (transformP.position.x > zoneZ.x && transformP.position.x < zoneZ.y)
                {
                    ContactPlayer contacts = stage.PlayerStage.StateController.GetContacts();
                    if (Mathf.Abs(posZoneY - transformP.position.y) < maxRadiusToZone && (contacts.Left || contacts.Right || contacts.Down))
                    {
                        anim = StartCoroutine(AnimMoveToEndElevator(contacts));
                        start = true;
                    }
                }
            }
        }
    }

    IEnumerator AnimMoveToEndElevator(ContactPlayer contacts)
    {
        if (PlayerExitFromStage != null)
        {
            PlayerExitFromStage();
        }

        TimeControll.Singleton.ChannelTimeTo(TimeControll.TimeNormal);
        stage.PlayerStage.StateController.BeginControll();
        stage.PlayerStage.StateController.PlayerStay();

        if (contacts.Left || contacts.Right)
        {
            while (!stage.PlayerStage.StateController.CheckToGround())
            {
                stage.PlayerStage.StateController.SetPosition(stage.PlayerStage.transform.position + new Vector3(0, -12f * Time.deltaTime, 0));
                yield return null;
            }
        }
        else
        {
            while (!stage.PlayerStage.StateController.CheckToGround())
            {
                yield return null;
            }
        }


        Vector3 startPos = stage.PlayerStage.transform.position;
        stage.PlayerStage.StateController.PlayerDirection(-Mathf.Sign((elevatorEnd.position - startPos).normalized.x));
        yield return new WaitForSecondsRealtime(0.1f);

        float dist = Vector3.Distance(elevatorEnd.position, startPos);


        float time = dist / 9.30f;
        int countT = (int)(time / 0.02f);

        stage.PlayerStage.StateController.PlayerRun();
        for (int i = 0; i < countT; i++)
        {
            stage.PlayerStage.transform.position = Vector3.Lerp(startPos, elevatorEnd.position, (float)i / (float)countT);
            yield return new WaitForSecondsRealtime(0.02f);
        }
        stage.PlayerStage.StateController.PlayerStay();

        yield return new WaitForSecondsRealtime(delayToStartElevator);
        countT = (int)(timeUp / 0.02f);

        for (int i = 0; i < countT; i++)
        {
            elevator.transform.localPosition = Vector3.Lerp(startLocalPos, endLocalPos, (float)i / (float)countT);
            stage.PlayerStage.StateController.SetPosition(elevator.TransformPoint(playerPosOnElevator));
            yield return new WaitForSecondsRealtime(0.02f);
        }

        stage.PlayerStage.StateController.EndControll();

        anim = null;
        yield break;
    }

    protected override void CallUnLoadStage()
    {

    }
}
