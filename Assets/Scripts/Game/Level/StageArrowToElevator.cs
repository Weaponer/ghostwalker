using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageArrowToElevator : StageEvent
{

    [SerializeField] private EndElevator endElevator;

    [SerializeField] private Vector3 localPositionElevator;

    bool show;

    bool gamePlay;

    protected override void CallLoadStage()
    {
        ArrowToElevator.Singleton.Hide();
        gamePlay = true;
        endElevator.PlayerExitFromStage += StopGame;
    }

    protected override void CallLateUpdateStage()
    {
        if (stage.GetEnemyOver() && gamePlay)
        {
            show = true;
            ArrowToElevator.Singleton.SetPositionAndDirect(localPositionElevator - stage.PlayerStage.transform.position);
            ArrowToElevator.Singleton.Show();
        }
    }

    private void StopGame() {
        gamePlay = false;
        ArrowToElevator.Singleton.Hide();
    }

    protected override void CallUnLoadStage()
    {
        if (show)
        {
            ArrowToElevator.Singleton.Hide();
        }
    }
}
