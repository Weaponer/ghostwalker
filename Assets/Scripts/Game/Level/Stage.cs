﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    public event System.Action StageComplit;

    [SerializeField] private float widthStage;

    [SerializeField] private Vector2 hegihtStage;

    [SerializeField] private Transform startElevator;

    [SerializeField] private Transform endElevator;

    [Space(15)]
    [SerializeField] private StageParamScene stageParam;

    [Space(15)]
    [SerializeField] private StageEvent[] stageEvents;

    public Player PlayerStage { get; private set; }

    private bool enemyOver;

    public Vector2 GetWidth()
    {
        Vector2 vector = new Vector2();
        vector.x = transform.position.z - widthStage / 2f;
        vector.y = transform.position.z + widthStage / 2f;
        return vector;
    }

    public Vector2 GetHeight()
    {
        return hegihtStage;
    }

    public Vector3 GetPositionSpawnPlayer()
    {
        return startElevator.position;
    }

    public void InitStage(Player player)
    {
        this.PlayerStage = player;
        EnemyControll.Singleton.EnemiesOver += EnemyKillOnStage;
        if (stageParam != null)
            stageParam.SetParams();

        PlayerStage.PlayerTeleport(startElevator.position);

        for (int i = 0; i < stageEvents.Length; i++) {
            stageEvents[i].LoadStage(this);
        }
    }

    private void Update()
    {
        if (enemyOver)
        {
            if (GetDistToEnd() < 2.3f)
            {
                if (StageComplit != null)
                    StageComplit();
                enemyOver = false;
            }
        }

        for (int i = 0; i < stageEvents.Length; i++)
        {
            stageEvents[i].UpdateStage();
        }
    }

    private void LateUpdate()
    {
        for (int i = 0; i < stageEvents.Length; i++)
        {
            stageEvents[i].LateUpdateStage();
        }
    }

    private void EnemyKillOnStage()
    {
        enemyOver = true;
    }

    public float GetDistToStart() {
        return Vector3.Distance(PlayerStage.transform.position, startElevator.position);
    }

    public bool GetEnemyOver()
    {
        return enemyOver;
    }

    public float GetDistToEnd()
    {
        return Vector3.Distance(PlayerStage.transform.position, endElevator.position);
    }

    public void DisableStage()
    {
        EnemyControll.Singleton.EnemiesOver -= EnemyKillOnStage;
        for (int i = 0; i < stageEvents.Length; i++)
        {
            stageEvents[i].UnLoadStage();
        }
    }
}
