using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartElevator : StageEvent
{
    public event System.Action PlayerExitFromElevatorEvent;

    [SerializeField] private Transform elevator;

    [SerializeField] private Vector3 startElevatorPosLocal;

    [SerializeField] private Vector3 endElevatorPosLocal;

    [SerializeField] private Vector3 posPlayerOnElevator;

    [SerializeField] private float time;

    [SerializeField] private Vector3 moveToPos;

    [SerializeField] private float timeMove;


    bool start;

    protected override void CallLoadStage()
    {
        stage.PlayerStage.StateController.BeginControll();
        elevator.transform.localPosition = startElevatorPosLocal;
        stage.PlayerStage.StateController.SetPosition(elevator.TransformPoint(posPlayerOnElevator));
    }

    protected override void CallLateUpdateStage()
    {
        if (!start && !Pause.IsPause) {
            TimeControll.Singleton.ChannelTimeTo(TimeControll.TimeNormal);
            StartCoroutine(StartGameAnim());
            start = true;
        }
    }

    IEnumerator StartGameAnim()
    {
        int countIt = (int)(time / 0.02f);
        stage.PlayerStage.StateController.PlayerStay();
        for (int i = 0; i < countIt; i++)
        {
            elevator.transform.localPosition = Vector3.Lerp(startElevatorPosLocal, endElevatorPosLocal, (float)i / (float)countIt);
            stage.PlayerStage.StateController.SetPosition(elevator.TransformPoint(posPlayerOnElevator));
            yield return new WaitForSecondsRealtime(0.02f);
        }

        GameHandler.Singleton.EventHandler.AddEvent(new TapToPlay());
        while (Pause.IsPause) {
            yield return null;
        }

        countIt = (int)(timeMove / 0.02f);
        stage.PlayerStage.StateController.PlayerRun();

        for (int i = 0; i < countIt; i++)
        {
            stage.PlayerStage.StateController.SetPosition(Vector3.Lerp(elevator.TransformPoint(posPlayerOnElevator), moveToPos, (float)i / (float)countIt));
            yield return new WaitForSecondsRealtime(0.02f);
        }
        stage.PlayerStage.StateController.PlayerStay();
        stage.PlayerStage.StateController.EndControll();

        if (PlayerExitFromElevatorEvent != null)
            PlayerExitFromElevatorEvent();
    }

    protected override void CallUpdateStage()
    {

    }

    protected override void CallUnLoadStage()
    {

    }
}
