﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Trajectory
{
    public static Vector3[] CreateTrajectory(int count, float timeDist, float force, Vector3 direct, float drag)
    {
        Vector3[] points = new Vector3[count];
        points[0] = Vector3.zero;
        Vector3 velocity = direct * force;
        Vector3 gravity = Physics.gravity;
        for (int i = 1; i < count; i++)
        {
            float dragCoef = velocity.magnitude;
            dragCoef = drag * dragCoef + drag * dragCoef * dragCoef;
            //  velocity += ((velocity * -dragCoef) * (1f / 20f)) * timeDist;

            velocity += gravity * timeDist;

            points[i] = points[i - 1] + velocity * timeDist;


        }

        return points;
    }
}
