﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    public static GameHandler Singleton { get; private set; }

    public GameEventHandler EventHandler { get; private set; }

    public BaseLevel Level { get { return levelControll; } }

    [SerializeField] private BaseLevel levelControll;
    private void Awake()
    {
        if (Singleton)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            Singleton = this;
        }

        EventHandler = new GameEventHandler();
        levelControll.StageComplete += StageComplite;
        levelControll.LevelComplete += LevelComplite;   
    }

    private void Start()
    {
        CameraControll.Singleton.InitGame();
        levelControll.StartGame();
    }

    private void Update()
    {
        EventHandler.Update();
    }

    public void PlayerDead()
    {
        EventHandler.AddEvent(new FaildLevel());
    }

    public void LevelComplite()
    {
        EventHandler.AddEvent(new CompleteLevel());
    }

    public void StageComplite()
    {
        EventHandler.AddEvent(new CompleteStage());
    }

    private void OnDestroy()
    {
        if (Singleton == this)
        {
            Singleton = null;
        }
    }

    public class GameEventHandler
    {
        List<EventGame> eventGames = new List<EventGame>();
        public void AddEvent(EventGame eventGame)
        {
            eventGames.Add(eventGame);
            eventGame.StartEvent();
            eventGame.EndEvent += RemoveEvent;
        }

        public void Update()
        {
            for (int i = 0; i < eventGames.Count; i++)
            {
                eventGames[i].Update();
            }
        }

        private void RemoveEvent(EventGame eventGame)
        {
            eventGames.Remove(eventGame);
        }
    }
}
