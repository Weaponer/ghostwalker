using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangedLevel : MonoBehaviour
{
    [SerializeField] Button go;

    [SerializeField] InputField numInput;

    private void Start()
    {
        go.onClick.AddListener(delegate { Changed(); });
    }

    private void Changed()
    {
        Level[] levels = SceneLoad.GetListLevels();
        int num = System.Convert.ToInt32(numInput.text);
        FileSystem.SaveFile(new GameFile() { CurrentLevel = num, CurrentStage = 0 });
        for (int i = 0; i < levels.Length; i++)
        {
            if (levels[i].NumerLevel == num)
            {
                SceneLoad.LoadLevel(levels[i]);
                return;
            }
        }
    }
}
