using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class EventSendControll
{
    public static void LevelStartEvent(int NumLevel, int NumStage)
    {
        Dictionary<string, object> param = new Dictionary<string, object>();
        param.Add("level " , NumLevel+ "." + NumStage);

        AppMetrica.Instance.ReportEvent("level_start", param);
        AppMetrica.Instance.SendEventsBuffer();
    }

    public static void LevelResultEvent(bool isFailed, float timeMS, int NumLevel, int NumStage) {
        Dictionary<string, object> param = new Dictionary<string, object>();
        param.Add("level ", NumLevel + "." + NumStage);
        param.Add("is_failed ", isFailed);
        param.Add("time_ms", timeMS);

        AppMetrica.Instance.ReportEvent("level_finish", param);
        AppMetrica.Instance.SendEventsBuffer();
    }
}
